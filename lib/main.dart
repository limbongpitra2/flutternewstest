import 'package:flutter/material.dart';
import 'package:fundamental/art.dart';
import 'package:fundamental/calculator.dart';
import 'package:fundamental/gesture.dart';
void main() {
  runApp( MyApp());
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => FirstSCreen(),
        '/secondScreen': (context) => secondScreen(),
        '/secondScreenWithData': (context) =>
            secondScreenData(ModalRoute.of(context)?.settings.arguments as String),
        '/returnDataScreen': (context) => ReturnDataScreen(),
        '/replacementScreen': (context) => ReplacementScreen(),
        '/anotherScreen': (context) => AnotherScreen(),
        '/art': (context) => Art(),
        '/gesture': (context) => Gesture(),
        '/calculator': (context) => Calculator(),
      },
    );
  }
}

class FirstSCreen extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Navigation'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: (){
                Navigator.pushNamed(context, '/secondScreen');
              }, 
              child: Text('Second Screen')
              ),
             ElevatedButton(
              child: Text('Navigation with Data'),
              onPressed: () {
                Navigator.pushNamed(context, '/secondScreenWithData',arguments: 'Hello ');
              },
            ),
            ElevatedButton(
              child: Text('Return Data from Another Screen'),
              onPressed: () async{
                final result = await Navigator.pushNamed(context, '/returnDataScreen');
                SnackBar snackBar = SnackBar(content: Text('$result'));
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              },
            ),
            ElevatedButton(
              child: Text('Replace Screen'),
              onPressed: () {
                  Navigator.pushNamed(context, '/replacementScreen');
              },
            ),
            ElevatedButton(
              child: Text('Art Screen'),
              onPressed: () {
                  Navigator.pushNamed(context, '/art');
              },
            ),
            ElevatedButton(
              child: Text('Art Screen'),
              onPressed: () {
                  Navigator.pushNamed(context, '/gesture');
              },
            ),
            ElevatedButton(
              child: Text('Calculator'),
              onPressed: () {
                  Navigator.pushNamed(context, '/calculator');
              },
            ),

          ],
        ),
      ),
    );
    
  }
}
class secondScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(

          child: Text('Back'),
          onPressed: (){
            Navigator.pop(context);
          },
          ),
      ),
    );
  }
}
class secondScreenData extends StatelessWidget{
  final String data ;

  const secondScreenData(this.data); 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(data),
            ElevatedButton(
              onPressed: (){
                Navigator.pop(context);
              }, 
              child: Text('Back')
              ),
          ],
        ),
      ),
    );
  }
}
class ReturnDataScreen extends StatefulWidget {
  @override
  _ReturnDataScreenState createState() => _ReturnDataScreenState();

}
class _ReturnDataScreenState extends State<ReturnDataScreen>{
  final _textController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: TextField(
                controller: _textController,
                decoration: InputDecoration(labelText: 'Enter Your Name : '),
              ),
              ),
              ElevatedButton(
                onPressed: (){
                   Navigator.pop(context, _textController.text);
                }, 
                child: Text('Send')
              ),
          ],
        ),
      ),
    );
  }
  void dispose(){
    _textController.dispose();
    super.dispose();
  }
}

 
class ReplacementScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(
          onPressed: (){
            Navigator.pushReplacementNamed(context, '/anotherScreen');
          }, 
          child: Text('Open Screen')
          ),
      ),
    );
  }
}
 
class AnotherScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Back to First Screen'),
            ElevatedButton(
              child: Text('Back'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}